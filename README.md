## moCronik's Drivable Wheelchair & Logo Mods

## About the mod pack
Adds
1. moCronikLogo
2. moCronikWheelchair

This mod pack makes it so you can pick up the current in game wheelchairs as a variant helper block which you can place down in any color available in the game or use as a part to make the drivable wheelchair!

Takes up to 5 mods: turbo, fuel saver, extra seat, lights and larger tank plus color it how you want!

In the workbench you can make both of the mods with some parts found in the world!

I was paralyzed in 2009 from an attempted suicide motorcycle accident and have struggled to get back to normal life. Gaming is one way to keep me happy and hopeful even with a painful and difficult life now.

So I have gotten into the modding to add a drivable wheelchair and my logo into the game.

## Installation
Download the "moCroniksMods.zip" file and extract the 2 folders starting with "moCronik" inside to your 7 Days to Die /Mods folder. If no /Mods folder than just create one and place the 2 mods folders in there.

Needs to be on server and client for multiplayer.

## Usage
Free to use. No modification or using in other mod packs without consent.

## Support
Contact me for help in my Discord at: https://discord.gg/34j6ymDdvD

## Support my content creation:
Merch: https://mocronik-shop.fourthwall.com

Tips: https://streamelements.com/mocronik-8842/tip

My Patreon: https://patreon.com/moCronik

## Follow me at:
YouTube: https://www.youtube.com/@mocronik

Discord: https://discord.gg/8BGJK2pte7

Twitter: https://twitter.com/moCronik

Steam: 50766738

## Roadmap
Close enough is close enough.

## Authors and acknowledgment
moCronik

## License
Open source License  *will update soon

## Changelog

Update v1.5.420: Updated code and made wheelchair faster than ever! Adjusted driver and passenger seating postions.

Update v1.4.20: Removed progression.xml and unlockedby tag in items.xml as was buggy on some overhaul mods.

Update v1.3.420: Added hopforce to make the wheelchair do bunny hops and lowered to 15 books to unlock the wheelchair recipe.

Update v1.2.420: Added Wheelchair into Science tab at the workbench so it now comes up when searching recipes.

Update v1.1.420: Added crafting the drivable wheelchair to the vehicle progression books and currently set to unlock at 20 vehicle adventures books read.

Inital v1.0.420: Release on 2/28/24 to full version to share with others.
